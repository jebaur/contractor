#!/bin/bash
# Helper script such that font url can be specifed in app config.

export FONT_URL=$(python -c \
                  "from app import app; print(app.config['FONT_URL'])" \
                  | tail -n 1)

exec "/entrypoint.sh" $@
