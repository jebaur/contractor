"""Fairguide data. Eventuelly should be in CRM. Until then here."""

from os import path
from textwrap import dedent


def _image(image):
    basepath = path.dirname(path.abspath(__file__))
    return path.join(basepath, 'images', image)


FAIRGUIDE_DATA = {

    'title': 'Kontakt.21',

    'copies': 800,  # "Auflage"

    # Images
    'title_image': _image('title.jpg'),
    'back_image':  _image('back.jpg'),
    'filler_base': _image('filler'),
    'filler_number': 5,

    # Images for missing logos/ads
    'logo_missing': _image('logo_missing.png'),
    'ad_missing': _image('ad_missing.png'),
    'ad_ad': _image('ad_ad.png'),

    # Notes at the end (an additional note page is inserted if the specified
    # number would result in and odd page number)
    'note_page_number': 2,

    # 'back_image'
    'tuesday_left': _image('tuesday_left.jpg'),
    'tuesday_right': _image('tuesday_right.jpg'),
    'wednesday_left': _image('wednesday_left.jpg'),
    'wednesday_right': _image('wednesday_right.jpg'),

    # Fair days (solve this with system locales maybe)
    # TODOOOO
    'first_day': '12',
    'second_day': '13',
    'first_weekday_de': 'Dienstag',
    'first_weekday_en': 'Tuesday',
    'second_weekday_de': 'Mittwoch',
    'second_weekday_en': 'Wednesday',
    'month': '10',
    'year': '2021',
    'start': '11:00',
    'end': '17:00',

    'president': 'Daniel Biek',
    'president_welcome_de': None,  # 2019: No intro in german
    'president_welcome_en': dedent("""
        Welcome to AMIV Kontakt.21!
        \medbreak

        The Kontakt.21 organization committee and I welcome you to this year’s
        job fair. We have some new and many interesting
        companies presenting themselves during those two days. I hope that you
        will learn something from interesting conversations with people from
        different working backgrounds and find the internship, job or thesis you wanted. 
	Take the opportunity to get an insight to the working world and also to improve your application documents. 

        The Kontakt is the yearly job fair of the student associations OBIS and
        AMIV, organised for students by students. Since twenty years it
        offers students the possibility to meet people from different
        companies. Whether you just want to get an overview where you could
        work later or concretely want to find a job, an internship or a master
        thesis, there are more than 40 companies you can talk to.
	A variety of different areas is offered, so you can receive impression of the area you like. 

        I would like to thank the Kontakt.21 committee which worked hard since
        last February to give you the best possible experience as well as
        everyone else that lent us a helping hand during the organization or
        the fair itself. 
        """),
    'president_image': _image('dani.jpg'),

    # TODO
    'rector': 'Sarah Springman',
    'rector_welcome_de': None,  # 2019: No intro in german
    'rector_welcome_en': dedent("""
        Dear students,
        \medbreak
        You are about to take your degrees and begin
        your professional lives. It is a wonderful
        opportunity to start to make your mark on the
        world, with your newly minted qualification as an
        engineer from ETH!
        Your degree opens up so many potential
        careers that it is sometimes a great challenge
        to be able to decide where to begin. There is so
        much choice, so it is important to speak with
        potential employers, and their recent recruits,
        and find out about their company.

        The \emph{AMIV Kontakt.21} event is not only an
        excellent opportunity to learn about a large
        number of well-known employers, but it also
        provides many chances to hear about the
        experience of those who have already made the
        transition from university to career.

        Ia am very happy indeed that the AMIV Kontakt.21 is planned
	to be held at ETH Zurich. Let us hope that an on-site event will be possible. 
	Visit AMIV Kontakt.19 to discover what fields
        the companies operate in, what you might
        expect in your future working environment, and
        what you should pay attention to when you
        apply. Ask your conversation partners about
        their entry into professional life, and the
        experiences that have remained with them. I
        am certain that you will enjoy many interesting
        encounters and discussions.

        I would like to show heartfelt appreciation to
        the Kontakt.21 team and the student associations,
        AMIV and OBIS, for organising this event.
        You have made the first step towards a
        successful career, post ETH, by volunteering to
        help others. Congratulations and thank you!

        I wish you all much success, joy and fulfilent in
        your future careers.
        """),
    'rector_image': _image('rector.jpg'),

    'booth_layout': _image('booth_layout.pdf'),

    'support_program': [
        ('Talk with Engineers Without Borders Switzerland (IngOG+),',
         '27.09.2021', '18:15', 'HG E 1.2', dedent(r"""
            \medbreak
            Meet the people behind Engineers Without Borders Switzerland (IngOG+),
            an association consisting of students and young professionals who
            voluntarily work on engineering projects around the world: from projects
            for social housing for women, drinking water for rural communities and
            schools to collaborations with foreign universities. Find out who we are,
            how we work and how to get involved!
            """)),
        ('Get ready for a job fair',
         '4.10.2021', '18:15', 'ML D 28', dedent(r"""
            \medbreak
            Are you ready to apply for the right job which fits your interests
            or are you overwhelmed by the endless application sites and don’t
            know how to get started? Maybe you are unsure if you are taking the
            right path? The Career Center will explain about job search
            strategies and decision making so you will know that you are making
            the right choices.
            """)),
        ('Lohnverhandlungen und Saläre',
         '7.10.2021', '18:15', 'HG E 1.1', dedent(r"""
            \medbreak
            \emph{Diese Veranstaltung findet in deutscher Sprache statt.}
            \medbreak
            Das Gehalt ist ein nicht zu unterschätzender Bestandteil des
            Jobangebots. Doch wie hoch ist ein klassisches Ingenieursgehalt
            wirklich? Swiss Engineering präsentiert Fakten zum Thema
            Lohnverhandlungen und Saläre und zu den damit verbundenen Aspekten.
            Damit beim nächsten Bewerbungsgespräch die Vorstellungen über ein
            angemessenen Lohn stimmen.
            """)),

    ],

    'cv_check_room': 'LEE E 308',
    'cv_check_intro_de': dedent(r"""
        Während der Messe sind mehrere Consultants
        der Consult \& Pepper AG anwesend
        und helfen, deinen CV auf Hochglanz
        zu polieren. Bring dazu einfach deinen
        ausgedruckten Lebenslauf mit.
        """),
    'cv_check_description_de': dedent("""
        Bei jeder Berbung ist der CV deine Visitenkarte.
        Er sollte auf alle Fragen zu deinen Qualifikationen, Erfahrungen
        und Kenntnissen beantworten und trotzdem kurz und prägnant sein.
        Häufig entscheidet eine erste Durchsicht der Bewerbungsdokumente
        durch den Arbeitgeber, ob die Kandidatin oder der Kandidat auf die
        zu besetzende Stelle passt oder nicht.
        Umso wichtiger ist es, dass dein CV ein professionelles
        Erscheinungsbild hat.

        Dabei helfen dir die Berater unseres Partners und unterstützen
        dich mit ihrer langjährigen Erfahrung.
        """),
    'cv_foto_de': dedent(r"""
        Zu jedem CV gehört auch ein professionelles
        Bewerbungsfoto. Mit deinem Profilbild
        hinterlässt du den ersten Eindruck, dein
        CV oder Motivationsschreiben überhaupt
        gelesen wird. Umso wichtiger ist es, dabei
        ein ansprechendes Foto zu haben, welches
        den Betrachter überzeugt.
        Deshalb bieten wir in Zusammenarbeit
        mit der Consult \& Pepper AG ein kostenloses Fotoshooting an.
        Eine Berufsfotografin rückt dich ins richtige
        Licht und wird dich von deiner besten
        Seite präsentieren.
        """),
    'cv_check_intro_en': dedent(r"""
        During the job fair, several consultants of our partner,
        the Consult \& Pepper AG, are available at your convenience
        and help to brush up your CV. Just bring a copy of it.
        """),
    'cv_check_description_en': dedent("""
        Your CV is the first impression an employer gets of you.
        It should show your qualifications, experiences and knowledge
        and still be comprehensive and concise.
        Oftentimes, the decision, if a candidate is invited for the
        interview, is made after an initial review of all the received
        CVs. It's all the more important that your CV has a professional
        appearance.

        The consultants of our partner support you with their experience
        of many years to stand out.
        """),
    'cv_foto_en': dedent("""
        Every CV requires a professional picture. With your profile,
        you leave the first impression before your CV or personal statement
        is read. This makes it all the more important to have an
        appealing picture to capture the viewer.

        Therefore, we offer a free fotoshooting with our partner, the
        Consult \& Pepper AG. A professional photographer will
        place you in the right light.
        """),

    # Pages for cv check company and career center
    'cv_check_company_id': 77,
    'career_center_id': 461,


    # Team
    'team_image': _image('team.jpg'),
    'kontakt_team': [
        ('Daniel Biek',
         _image('dani.jpg'),
         r'Präsident\minilangsep President'),
        ('Celine Erzberger',
         _image('celine.jpg'),
         r'Quästor\minilangsep Treasurer'),
        ('Sandro Lutz',
         _image('sandro.jpg'),
         r'IT-Vorstand\minilangsep\\Head of IT'),
     	('Jakob Woehler',
         _image('jakob.jpg'),
         r'IT'),
        ('Aneska Heidemüller',
         _image('aneska.jpg'),
         r'Infrastruktur\minilangsep\\Infrastructure'),
        ('George Motschan-Armen',
         _image('george.jpg'),
        r'Infrastruktur\minilangsep\\Infrastructure'),
        ('Paul Wolff',
         _image('paul.jpg'),
         r'Catering'),
        ('Marius Siebenaller',
         _image('marius.jpg'),
          r'Catering'),
        ('Martin Stecher',
         _image('martin.jpg'),
         r'PR'),
        ('Sven Gutjahr',
         _image('sven.jpg'),
         r'PR')
    ],

    'er_team': [
        'Kira Erb',
        'Ciril Bullinger',
        'Silvio Geel',
        'Selcuk Taskin',
        'Tim Gmünder',
        'Aneska Heidemüller',
        'Deniz Cinar',
        r'\vspace{5cm}',  # Hack to make columns look nice
    ],

    # Additional Data for info letter only
    'supervisor': 'Daniel Biek',
    'supervisor_phone': '+41 76 345 43 55',

    'coffee_room': 'CLA D 19',
    'wifi_name': None,  # 'public',
    'wifi_user': None,  # 'kontakt.18',
    'wifi_password': None,  # 'AmivJobFair18',

    'map': _image('map.png'),

    'setup_start': '8:30',
    'setup_end': '11:00',
    'teardown_start': '17:00',
    'teardown_end': '18:00',
    'apero_start': '17:15',
    'apero_end': '19:30',

    # Data for CV Check Contract
    'cv_check_price': 1000,

}
