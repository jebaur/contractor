"""Test the fairguidepage API."""

import pytest
from unittest.mock import patch
from tempfile import TemporaryDirectory

from werkzeug.exceptions import BadRequest

from app import app as base_app, fairguidepage
from contractor import api


@pytest.fixture
def app():
    """Adjust storage directory before yielding app object."""
    with TemporaryDirectory() as tempdir:
        base_app.config['STORAGE_DIR'] = tempdir
        yield base_app


@pytest.fixture
def client(app):
    """Return an app client."""
    yield app.test_client()


@pytest.fixture()
def demo_data():
    """Correct data for a complete company page."""
    return {
        'name': "Testcorp",
        'website': 'test.corp.com',
        'contact': 'Mr. M. Anager\nm.anager@testcorp.com',
        'employees_ch': 42,
        'employees_world': 420,

        'booth': 'A32',
        'interested_in': ['itet', 'mavt', 'mtec'],
        'offers': ["bachelor", "master", "semester",
                   "fulltime", "internship", "trainee"],

        'about': 'Eine sehr aufschlussreiche Beschreibung.',
        'focus': 'Gefolgt von nützlichen Details.',

        'logo': 'https://mdn.mozillademos.org/files/3075/svgdemo1.xml',
        'ad': 'http://www.orimi.com/pdf-test.pdf',
    }


def test_no_json(app):
    """Test that parsing fails if some else that json is sent."""
    with app.test_request_context(data={'key': 'value'}), \
            pytest.raises(BadRequest):
        fairguidepage()


def test_all_data(app, demo_data):
    """Test that the form is validated correctly."""
    with app.test_request_context():
        api.parse_company_data(demo_data)


def test_request(client, demo_data):
    """Test an actual request, save file for manual check."""
    with open('checkme.pdf', 'wb') as file:
        response = client.post('/fairguidepage', json=demo_data)
        assert response.status_code == 200, str(response.data)
        file.write(response.data)


def test_cache(client, demo_data):
    """Test that subsequent requests are cached."""
    test_response = b'Test byte stuff'
    with patch('app.get_data', return_value=test_response) as mock:
        response = client.post('/fairguidepage', json=demo_data)
        assert response.status_code == 200, str(response.data)
        assert mock.called
        assert response.data == test_response

        # Test another time with same input, data should come from cache
        mock.reset_mock()
        response = client.post('/fairguidepage', json=demo_data)
        assert response.status_code == 200, str(response.data)
        assert not mock.called
        assert response.data == test_response

        # Test another time with differnt input, data not from cache
        mock.reset_mock()
        demo_data['name'] = 'different'
        response = client.post('/fairguidepage', json=demo_data)
        assert response.status_code == 200, str(response.data)
        assert mock.called
        assert response.data == test_response


def test_download_ad(app):
    """Test that a svg download does not crash (very primitive, I know)."""
    url = 'http://www.orimi.com/pdf-test.pdf'
    with app.app_context():
        api._get_ad(url)


def test_download_logo(app):
    """Test that a svg download does not crash (very primitive, I know)."""
    url = 'https://mdn.mozillademos.org/files/3075/svgdemo1.xml'
    with app.app_context():
        api._get_logo(url)
