"""A simple api to request documents."""
import json
import subprocess
from os import makedirs, path

import requests
from flask import abort, current_app
from werkzeug.exceptions import NotFound

from schema import And, Optional, Or, Schema, SchemaError, Use

from contractor.utils import hash_data
from contractor.fairguide_data import FAIRGUIDE_DATA


def api_request(endpoint, **headers):
    """Send an authorized request to the kontakt api and parse json response."""
    url = current_app.config['KONTAKTAPI_URL'] + endpoint
    headers = {
        'Authorization': current_app.config['KONTAKTAPI_KEY'],
        **headers
    }
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response.json()


def get_company_list():
    """Return a list of company names and ids only."""
    response = api_request('companies/all')

    return [{
        'name': company['company_name'],
        'id': company['id'],
    } for company in response]


def get_companies(company_id=None, ignore_participation=False):
    """TODO: Return all companies.

    Currently, many companies are only returned with errors. At this point,
    using this function for all companies is not recommended.

    `ignore participation` is a hacky thing to be able to load
    the CV check company and the career center, as the data format is (yet)
    a bit inconsitent here.
    """
    if company_id is not None:
        return [get_company(company_id,
                            ignore_participation=ignore_participation)]

    # First get all ids, then companies
    response = api_request('companies/all')

    _ids = [(c['id'], c['company_name']) for c in response]
    all_data = []
    errors = {}
    for _id, name in _ids:
        # Also hacky: Exclude CV Check Company and Career Center
        if _id in [FAIRGUIDE_DATA['cv_check_company_id'],
                   FAIRGUIDE_DATA['career_center_id']]:
            continue

        try:
            all_data.append(get_company(_id))
        except NotFound:
            errors[name] = "No data received from the kontakt API"
        except SchemaError as e:
            errors[name] = str(e)

    if errors:
        raise Exception('Not all companies could be loaded! %s' %
                        json.dumps(errors))

    return all_data


def get_company(company_id, ignore_participation=False):
    """Return all data for companies.

    Returns:
        tuple(dict, string): company_data, hash id
    """
    response = api_request('company_info/' + str(company_id))
    # if response.status_code != 200:
    #    abort(404)
    return _parse_response(response,
                           ignore_participation=ignore_participation)


def _parse_response(data, ignore_participation=False):
    """Validate and format data for company.

    Check if all information is provided, and process images.
    The logo is expected as .svg, and is downloaded and converted to pdf.
    The ad is exepcted as .pfg.

    Args:
        data (dict): The company data.
        download (bool): Whether to download files.

    Returns:
        dict: company information, with filepaths for media.

    Raises:
        BadRequest: Data cannot be parsed as json (status code 400).
        UnprocessableEntity: Validation Error (code 422). Error message in body.
    """
    if ignore_participation:
        particiation = {'participation': Schema({}, ignore_extra_keys=True)}
    else:
        particiation = {'participation': {
            "packages": [{
                'id': int,
                'cost': Use(int),  # Prices are always in full CHF.
                'title_de': str,
                'description_de': str,
                'description_en': str,
                'title_en': str,
            }],
            "booths": [{
                'id': int,
                'category': Or("A", "B", "C"),
                'day': Use(_format_days),
                'size': Use(_format_size),
                'tables': Use(_format_tables),
                # Prices are always in full francs
                'price': Use(int),
                # The assigned booth
                Optional('location'): And(str, Use(lambda x: x.strip())),
            }],
            # Prices are always in full francs
            "price_total": Use(int),
        }}

    schema = And(
        # 1. Pre-Processing of all data
        Use(_clean),

        # 2. Parsing the individual fields.
        Schema({
            # Top of page info
            "name": str,
            Optional("website"): str,
            Optional("contact"): str,
            # Employees (at least one must be mentioned)
            Optional(Or("employees_ch", "employees_world")): Use(int),

            # Boxes on the side
            Optional("interested_in"): And(["itet", "mavt", "mtec"],
                                           Use(_format_interests)),
            Optional("offers"): And(["bachelor", "master", "semester",
                                     "fulltime", "internship", "trainee"],
                                    Use(_format_offers)),

            # Description texts
            Optional("about"): str,
            Optional("focus"): str,

            # Logo and Ad (both must be urls to download files from)
            Optional('logo'): And(str, Use(_download('svg')), Use(_svg2pdf)),
            Optional('has_ad'): bool,
            Optional('ad'): And(str, Use(_download('pdf'))),

            # Contract data
            Optional("contract_amiv_responsible"): str,
            "contract_city": str,
            Optional("contract_country", default=""): str,
            "contract_email_address": str,
            "contract_partners": [str],
            "contract_postal_code": str,
            "contract_street": str,

            # Participation
            **particiation,
        }, ignore_extra_keys=True),

        # 3. Postprocessing of all data
        Use(_format_employees),
        Use(_compute_booth_index),
        Use(_set_ready),

    )

    return schema.validate(data)


def _set_ready(data):
    """Set fairguide_ready key."""
    data.setdefault('fairguide_ready', True)
    return data


def _clean(data):
    """Remove None and empty strings, they are treated like missing values."""
    if isinstance(data, dict):
        return {key: _clean(value) for key, value in data.items()
                if _issomething(value)}
    elif isinstance(data, list):
        return [_clean(item) for item in data if _issomething(item)]
    else:
        return data


def _issomething(value):
    """Check whether the value is not None or an empty string."""
    return value not in ["", None]


def _format_size(value):
    """Map booth size integer to an understandable value."""
    return {0: 'small', 1: 'big'}[int(value)]


def _format_tables(value):
    """Map table integer to an understandable value."""
    return {0: 'bar', 1: 'standard'}[int(value)]


def _format_days(value):
    """Map table integer to an understandable value."""
    return {1: 'first', 2: 'second', 3: 'both'}[int(value)]


def _format_interests(values):
    """Make uppercase and sort to ensure consistency."""
    return sorted(item.upper() for item in values)


def _format_offers(data):
    """Sort and internationalize offers (currently german only)."""
    offers = {}
    thesis_offers = [thesis.capitalize()
                     for thesis in ["bachelor", "master", "semester"]
                     if thesis in data]
    # If more then one item, put last on newline
    if len(thesis_offers) > 1:
        thesis_offers[-1] = "\n%s" % thesis_offers[-1]
    # Join them with kommas and write 'arbeiten' after the last one
    if thesis_offers:
        offers["thesis"] = "%sarbeiten" % ", ".join(thesis_offers)

    if "fulltime" in data:
        offers["fulltime"] = "Festanstellungen"

    entry_map = [("internship", "Praktika"), ("trainee", "Traineeprogramm")]
    entry_offers = [item for key, item in entry_map if key in data]
    if entry_offers:
        offers["entry"] = "\n".join(entry_offers)

    return offers


def _format_employees(data):
    """Combine swiss and worldwide employees into a single field."""
    def _format(key, suffix):
        """Re-format numbers and add suffix."""
        return "{:,} {}".format(data[key], suffix) if key in data else None

    swiss = _format('employees_ch', 'Schweiz\\minilangsep Switzerland')
    world = _format('employees_world', 'weltweit\\minilangsep worldwide')
    data['employees'] = r'\\ '.join(filter(None, [swiss, world]))
    return data


def _compute_booth_index(data):
    """Turn the booth into a sortable index, s.t. A12 < A2 etc."""
    booths = data.get('participation', {}).get('booths', [])

    for booth in booths:
        location = booth.get('location')
        if location is not None:
            location = location.split('/')[0]  # second booth not needed for sort
            # Convert Number + int to pure integer
            booth['booth_index'] = ord(location[0]) * 100 + int(location[1:3])
        else:
            booth['booth_index'] = 0
            booth['location'] = 'None'
    return data


def _download(filetype):
    """Create a download function for a given filetype.

    If the file exists, it is not downloaded again.
    """
    def _download_with_type(url):
        """Download a file with given type."""
        filepath = path.join(
            current_app.config["STORAGE_DIR"],
            "%s.%s" % (hash_data(url), filetype))

        # If file exists, return immediately, otherwise download.
        if not path.exists(filepath):
            makedirs(current_app.config["STORAGE_DIR"], exist_ok=True)
            with requests.get(url, stream=True) as response, \
                    open(filepath, 'wb') as file:
                if response.status_code != 200:
                    # TODO: Can we will missing files with placeholders?
                    return None
                    raise RuntimeError("File could not be downloaded.")

                for chunk in response:
                    file.write(chunk)

        return filepath
    return _download_with_type


def _svg2pdf(filepath):
    """Convert svg file to pdf. Return filepath of pdf file."""
    new_filepath = filepath.replace('.svg', '.pdf')

    # If file exists, return immediately, otherwise convert.
    if not path.exists(new_filepath):
        arg_string = current_app.config['CONVERT_CMD'].format(
            input=filepath,
            output=new_filepath)
        try:
            subprocess.check_output(
                arg_string.split(), stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as error:
            raise RuntimeError(error.output)

    return new_filepath
